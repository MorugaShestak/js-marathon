const item = document.querySelector(".item")
const placeholders = document.querySelectorAll(".placeholder")

item.addEventListener("dragstart", e => {
    e.target.classList.add("hold")
    setTimeout(() => {e.target.classList.add("hide")}, 10)

})

item.addEventListener("dragend", e => {
    e.target.classList.remove("hold", "hide")
})

let dragover = (e) => {
    e.preventDefault()
}

let dragenter = (e) => {
    if(e.target.classList.contains("startt") === true) {
        e.target.classList.add("hovered-start")
    }
    else if(e.target.classList.contains("progresss") === true) {
        e.target.classList.add("hovered-progress")
    }
    else if(e.target.classList.contains("donee") === true) {
        e.target.classList.add("hovered-done")
    }

}

let dragleave = (e) => {
    if(e.target.classList.contains("startt") === true) {
        e.target.classList.remove("hovered-start")
    }
    else if(e.target.classList.contains("progresss") === true) {
        e.target.classList.remove("hovered-progress")
    }
    else if(e.target.classList.contains("donee") === true) {
        e.target.classList.remove("hovered-done")
    }
}

let dragdrop = (e) => {
    e.target.append(item)
    e.target.classList.remove("hovered")
}

placeholders.forEach((placeholder) => {
    placeholder.addEventListener("dragover",dragover)
    placeholder.addEventListener("dragenter", dragenter)
    placeholder.addEventListener("dragleave", dragleave)
    placeholder.addEventListener("drop", dragdrop)
})

