const startBtn = document.querySelector("#start")
const screens = document.querySelectorAll(".screen")
const timeList = document.querySelector("#time-list")
const timeEl = document.querySelector("#time")
let time = 0;
let score = 0;
const colors = ["#1c301e",
    "#de47df",
    "#88eb40",
    "#9d40e1",
    "#54e358",
    "#4d24b3",
    "#c6e63e",
    "#6d56e0",
    "#59b538",
    "#a737b5",
    "#7bde7e",
    "#d838aa",
    "#59e6a9",
    "#6c2791",
    "#c8c736",
    "#302387",
    "#d6e97c",
    "#4465db",
    "#e3b636",
    "#7186ed",
    "#e28c23",
    "#373d88",
    "#5d9830",
    "#c474e5",
    "#387024",
    "#da6cc2",
    "#58ab71",
    "#d73985",
    "#68ddc7",
    "#e1352b",
    "#5dd2e4",
    "#e25f24",
    "#4899df",
    "#8d8c1f",
    "#825ebd",
    "#a2b65d",
    "#38135a",
    "#e4db97",
    "#201942",
    "#bbdfa5",
    "#8a2d7c",
    "#dfb768",
    "#4464af",
    "#da8b4d",
    "#7990df",
    "#a48031",
    "#ba9de6",
    "#344e1f",
    "#e93b67",
    "#41805b",
    "#bd2e46",
    "#52a2c3",
    "#a73721",
    "#9ac9d2",
    "#72221f",
    "#bde4cf",
    "#4c1544",
    "#e5d0bc",
    "#0d1724",
    "#e56c5b",
    "#457196",
    "#9f5625",
    "#abbeec",
    "#6f4817",
    "#e097d5",
    "#717a37",
    "#a468a9",
    "#cfb187",
    "#291122",
    "#d1c7d8",
    "#4c1a21",
    "#e6b5df",
    "#34231f",
    "#eda286",
    "#1f3443",
    "#e3719c",
    "#599793",
    "#a4365c",
    "#96a281",
    "#75284f",
    "#3a5b4d",
    "#cd7273",
    "#364870",
    "#b78061",
    "#624179",
    "#7f643c",
    "#7a91bf",
    "#493722",
    "#e8a6b2",
    "#473350",
    "#b38c8d",
    "#36606e",
    "#a86281",
    "#736b53",
    "#6f6b9c",
    "#864b45",
    "#ae97bb",
    "#6d515f",
    "#858090",
    "#87668a"];

document.getElementById("board").addEventListener("click", e => {
    if(e.target.classList.contains("circle")) {
        score = score + 1;
        e.target.remove()
        createRandomCircle()
    }
})

startBtn.addEventListener("click", e => {
    e.preventDefault()
    screens[0].classList.add("up")
})

timeList.addEventListener("click", e => {
    if(e.target.classList.contains("time-btn")) {
        screens[1].classList.add("up")
        time = parseInt(e.target.getAttribute("data-time"));
        startGame(time)
    }
})

function startGame(time) {
    setInterval(decreaseTime, 1000)
    setTime(time)
    createRandomCircle()
}

function decreaseTime() {
    if (time === 0) {
        finishGame()
    } else {
        let current = --time;
        if (current < 10) {
            current = `0${current}`
        }
        setTime(current)
    }
}

function setTime(val) {
    timeEl.innerHTML = `00:${val}`
}

function finishGame() {
    document.getElementById("board").innerHTML = `<h1>Ваш счёт: <span class="primary">${score}</span></h1>`;
    timeEl.parentNode.classList.add("hide");
}

function createRandomCircle() {
    const circle = document.createElement("div")
    circle.classList.add("circle")
    circle.style.background=colors[Math.floor(Math.random()*colors.length)]
    let size = getRandomNumber(16, 490)
    const {width, height} = document.getElementById("board").getBoundingClientRect()
    let x = getRandomNumber(0, width - size);
    let y = getRandomNumber(0, height - size);

    circle.style.height = `${size}px`;
    circle.style.width = `${size}px`;
    circle.style.top = `${y}px`
    circle.style.left = `${x}px`

    circle.addEventListener("click", () => {

    })

    document.querySelector("#board").append(circle)
}

function getRandomNumber(min,max) {
    return Math.floor(Math.random() * ((max-min) + min))
}