function init() {
    let slides = document.querySelectorAll(".slide");
    slides.forEach((e) => {
        e.addEventListener("click", () => {
            if(e.classList.contains("active") === false) {
                clear(slides)
                active(e)
            }
            else {
                e.classList.remove("active")
            }
        })
    })
}

function active(e) {
    e.classList.add("active")
}

function clear(slides) {
    slides.forEach(e => {
        e.classList.remove("active")
    })
}