# [js-marathon](https://result.school/products/marathon-js)

## All 5 lessons from js-marathon

### Day 1 - Card Gallery | [codepen](https://codepen.io/MorugaShestak/pen/ExpXGae)
### Day 2 - Drag&Drop | [codepen](https://codepen.io/MorugaShestak/pen/MWBoZoX)
### Day 3 - Image slider | [codepen](https://codepen.io/MorugaShestak/pen/NWBgQag)
### Day 4 - Board mini-game | [codepen](https://codepen.io/MorugaShestak/pen/ExpvyYZ)
### Day 5 - Aim Game | [codepen](https://codepen.io/MorugaShestak/pen/qByXRXx)